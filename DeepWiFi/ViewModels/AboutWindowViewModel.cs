﻿using DeepWiFi.Tools;
using DeepWiFi.ViewModels.Commands;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace DeepWiFi.ViewModels
{
    public class AboutWindowViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AboutWindowViewModel"/> class
        /// </summary>
        public AboutWindowViewModel()
        {
            ReadAttributes(Assembly.GetExecutingAssembly());

            CloseWindowCommand = new RelayCommand(
                x =>
                {
                    if (x is Window window)
                    {
                        window.Close();
                    }
                }
            );
        }

        /// <summary>
        /// Gets or sets the product name
        /// </summary>
        public string Product { get; private set; }

        /// <summary>
        /// Gets or sets the application author
        /// </summary>
        public string Author { get; private set; }

        /// <summary>
        /// Gets or sets the application version
        /// </summary>
        public string Version { get; private set; }

        /// <summary>
        /// Gets or sets the copyright
        /// </summary>
        public string Copyright { get; private set; }

        /// <summary>
        /// Gets or sets the application description
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the OK command
        /// </summary>
        public ICommand CloseWindowCommand { get; }

        /// <summary>
        /// Reads the Attributes of the committed assembly
        /// </summary>
        /// <param name="assembly"></param>
        private void ReadAttributes(Assembly assembly)
        {
            Product = (assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false).FirstOrDefault() as AssemblyProductAttribute)?.Product;
            Author = (assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false).FirstOrDefault() as AssemblyCompanyAttribute)?.Company;
            Version = assembly.GetName().Version.ToString(3);
            Copyright = (assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false).FirstOrDefault() as AssemblyCopyrightAttribute)?.Copyright;
            //Description = (assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false).FirstOrDefault() as AssemblyDescriptionAttribute)?.Description;
            Description = LanguageManager.GetTranslated("ApplicationDescription");
        }
    }
}
