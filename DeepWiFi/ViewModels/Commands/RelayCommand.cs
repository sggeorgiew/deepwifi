﻿using System;
using System.Windows.Input;

namespace DeepWiFi.ViewModels.Commands
{
    /// <summary>
    /// Generic relay command for view models
    /// </summary>
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class
        /// </summary>
        /// <param name="execute"></param>
        public RelayCommand(Action<object> execute) : this(execute, null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            ExecuteDelegate = execute;
            CanExecuteDelegate = canExecute;
        }
        
        /// <summary>
        /// Gets or sets the execute delegate
        /// </summary>
        public Action<object> ExecuteDelegate { get; private set; }

        /// <summary>
        /// Gets or sets the can execute delegate
        /// </summary>
        public Predicate<object> CanExecuteDelegate { get; private set; }

        /// <summary>
        /// Checks whether the command can be executed
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return CanExecuteDelegate == null || CanExecuteDelegate(parameter);
        }

        /// <summary>
        /// Updates the can execute status
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the command
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            ExecuteDelegate(parameter);
        }
    }
}
