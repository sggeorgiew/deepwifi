﻿using DeepWiFi.Common;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DeepWiFi.ViewModels.Converters
{
    /// <summary>
    /// Network authentication mode converter
    /// </summary>
    public class AuthenticationConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return DependencyProperty.UnsetValue;

            if (value is Authentication authentication)
            {
                return authentication switch
                {
                    Authentication.Open => "Open",
                    Authentication.WEP => "WEP",
                    Authentication.WPAPersonal => "WPA Personal",
                    Authentication.WPAEnterprice => "WPA Enterprice",
                    Authentication.WPA2Personal => "WPA2 Personal",
                    Authentication.WPA2Enterprice => "WPA2 Enterprice",
                    Authentication.Unknown => "Unknown",
                    _ => string.Empty
                };
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts a value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
