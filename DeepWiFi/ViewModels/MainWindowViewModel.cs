﻿using DeepWiFi.Common;
using DeepWiFi.Models;
using DeepWiFi.Tools;
using DeepWiFi.ViewModels.Commands;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DeepWiFi.ViewModels
{
    /// <summary>
    /// Main window view model
    /// </summary>
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IDialogCoordinator dialogCoordinator;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class
        /// </summary>
        /// <param name="instance">Dialog coordinator instance</param>
        public MainWindowViewModel(IDialogCoordinator instance)
        {
            dialogCoordinator = instance;

            Languages = new List<Language>()
            {
                new Language("en-US", "EN"),
                new Language("bg-BG", "BG"),
            };

            Networks = new ObservableCollection<Network>();

            LanguageDropDownMenuItemCommand = new RelayCommand(
                x =>
                {
                    string selectedLanguage = x.ToString();
                    if (selectedLanguage != LanguageManager.CurrentLanguage)
                    {
                        LanguageManager.ChangeLanguage(selectedLanguage);

                        foreach (Network network in Networks)
                        {
                            network.Translate();
                        }

                        Properties.Settings.Default.SelectedLanguage = selectedLanguage;

                        foreach (Language language in Languages)
                        {
                            language.UpdateStatus();
                        }
                    }
                }
            );

            AboutCommand = new RelayCommand(
                x =>
                {
                    _ = new AboutWindow(Application.Current.MainWindow).ShowDialog();
                }
            );

            DonateCommand = new RelayCommand(
                x =>
                {
                    ProcessManager.Open("https://www.paypal.com/paypalme/sgeorgiew");
                }
            );

            ShowHidePasswordCommand = new RelayCommand(
                async x =>
                {
                    try
                    {
                        System.Collections.IList items = (System.Collections.IList)x;
                        if (items.Count == 0)
                        {
                            return;
                        }

                        IEnumerable<Network> selectedNetworks = items.Cast<Network>();
                        foreach (Network network in selectedNetworks)
                        {
                            network.IsPasswordHidden = !network.IsPasswordHidden;
                        }
                    }
                    catch (Exception)
                    {
                        await dialogCoordinator.ShowMessageAsync(this,
                            LanguageManager.GetTranslated(Constants.NetworkManager),
                            LanguageManager.GetTranslated(Constants.SomethingWentWrong));
                    }
                },
                o => o != null && ((System.Collections.IList)o).Count > 0
            );

            ExportNetworkCommand = new RelayCommand(
                async x =>
                {
                    try
                    {
                        System.Collections.IList items = (System.Collections.IList)x;
                        if (items.Count != 1)
                        {
                            return;
                        }

                        CommonOpenFileDialog folderDialog = new CommonOpenFileDialog()
                        {
                            InitialDirectory = Environment.SpecialFolder.DesktopDirectory.ToString(),
                            Title = LanguageManager.GetTranslated("ExportNetwork"),
                            IsFolderPicker = true,
                            RestoreDirectory = true,
                            EnsurePathExists = true
                        };

                        if (folderDialog.ShowDialog() == CommonFileDialogResult.Ok)
                        {
                            if (items[0] is Network network)
                            {
                                ResultData<bool> result = await NetworkManager.ExportNetworkProfile(network, folderDialog.FileName);
                                if (result.Data)
                                {
                                    await dialogCoordinator.ShowMessageAsync(this,
                                        LanguageManager.GetTranslated(Constants.NetworkManager),
                                        LanguageManager.GetTranslated("ExportNetworkSuccess"));
                                }
                                else
                                {
                                    await dialogCoordinator.ShowMessageAsync(this,
                                        LanguageManager.GetTranslated(result.Error.Key),
                                        result.Error.Value);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        await dialogCoordinator.ShowMessageAsync(this,
                            LanguageManager.GetTranslated(Constants.NetworkManager),
                            LanguageManager.GetTranslated(Constants.SomethingWentWrong));
                    }
                },
                o => o != null && ((System.Collections.IList)o).Count == 1
            );

            ImportNetworkCommand = new RelayCommand(
                async x =>
                {
                    try
                    {
                        OpenFileDialog openFileDialog = new OpenFileDialog()
                        {
                            InitialDirectory = Environment.SpecialFolder.DesktopDirectory.ToString(),
                            Title = LanguageManager.GetTranslated("ImportNetwork"),
                            Filter = "Extensible Markup Language (*.xml)|*.xml",
                            RestoreDirectory = true
                        };

                        if (openFileDialog.ShowDialog() == true)
                        {
                            ResultData<Network> result = await NetworkManager.ImportNetworkProfile(openFileDialog.FileName);
                            if (result.Data != null)
                            {
                                result.Data.SetNetworkProfile();
                                result.Data.Id = (Networks.LastOrDefault()?.Id ?? 0) + 1;

                                Networks.Add(result.Data);

                                await dialogCoordinator.ShowMessageAsync(this,
                                    LanguageManager.GetTranslated(Constants.NetworkManager),
                                    LanguageManager.GetTranslated("ImportNetworkSuccess"));
                            }
                            else
                            {
                                await dialogCoordinator.ShowMessageAsync(this,
                                    LanguageManager.GetTranslated(result.Error.Key),
                                    LanguageManager.GetTranslated(result.Error.Value));
                            }
                        }
                    }
                    catch (Exception)
                    {
                        await dialogCoordinator.ShowMessageAsync(this,
                            LanguageManager.GetTranslated(Constants.NetworkManager),
                            LanguageManager.GetTranslated(Constants.SomethingWentWrong));
                    }
                }
            );

            DeleteNetworkCommand = new RelayCommand(
                async x =>
                {
                    try
                    {
                        System.Collections.IList items = (System.Collections.IList) x;
                        if (items.Count == 0)
                        {
                            return;
                        }

                        MessageDialogResult dialogResult;
                        if (items.Count > 1)
                        {
                            dialogResult = await dialogCoordinator.ShowMessageAsync(this,
                                        LanguageManager.GetTranslated("NetworkManager"),
                                        string.Format(
                                            LanguageManager.GetTranslated("RemoveNetworksQuestionMessage"), 
                                            items.Count),
                                        MessageDialogStyle.AffirmativeAndNegative);
                        }
                        else
                        {
                            dialogResult = await dialogCoordinator.ShowMessageAsync(this,
                                LanguageManager.GetTranslated("NetworkManager"),
                                string.Format(
                                    LanguageManager.GetTranslated("RemoveNetworkQuestionMessage"),
                                    items[0] is Network network ?  network.SSID : LanguageManager.GetTranslated(Constants.Unknown)), 
                                MessageDialogStyle.AffirmativeAndNegative);
                        }

                        if (dialogResult == MessageDialogResult.Negative)
                        {
                            return;
                        }

                        int errorCount = 0;
                        IEnumerable<Network> selectedNetworks = items.Cast<Network>();
                        for (int i = 0; i < selectedNetworks.Count(); i++)
                        {
                            Network network = selectedNetworks.ElementAt(i);

                            ResultData<bool> result = await NetworkManager.DeleteNetworkProfile(network);
                            if (!result.Data)
                            {
                                errorCount++;
                            }

                            Networks.Remove(network);
                        }

                        if (errorCount > 0)
                        {
                            await dialogCoordinator.ShowMessageAsync(this,
                                LanguageManager.GetTranslated(Constants.NetworkManager), 
                                string.Format(
                                    LanguageManager.GetTranslated("RemoveNetworksErrorMessage"),
                                    errorCount), 
                                MessageDialogStyle.Affirmative);
                        }
                    }
                    catch (Exception)
                    {
                        await dialogCoordinator.ShowMessageAsync(this,
                            LanguageManager.GetTranslated(Constants.NetworkManager), 
                            LanguageManager.GetTranslated(Constants.SomethingWentWrong));
                    }
                },
                o => o != null && ((System.Collections.IList)o).Count > 0
            );

            ScanNetworksCommand = new RelayCommand(
                async x =>
                {
                    if (Networks?.Count > 0)
                    {
                        Networks.Clear();
                    }

                    await GetNetworks();
                }
            );

            SaveNetworksCommand = new RelayCommand(
                async x =>
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog()
                    {
                        InitialDirectory = Environment.SpecialFolder.DesktopDirectory.ToString(),
                        Filter = "Text files (*.txt)|*.txt",
                        RestoreDirectory = true,
                        FileName = "wifi-networks.txt"
                    };

                    if (saveFileDialog.ShowDialog() == true)
                    {
                        using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8))
                        {
                            sw.WriteLine(Constants.DeepWiFi);

                            foreach (Network network in Networks)
                            {
                                sw.WriteLine($"#{network.Id} | SSID: {network.SSID} | Password: {network.GetPassword()} | Authentication: {network.Authentication}");
                            }
                        }

                        await dialogCoordinator.ShowMessageAsync(this, 
                            LanguageManager.GetTranslated(Constants.NetworkManager),
                            LanguageManager.GetTranslated("SaveNetworksSuccess"));
                    }
                },
                o => Networks.Count > 0
            );

            CloseCommand = new RelayCommand(
                x =>
                {
                    Environment.Exit(0);
                }
            );
        }

        /// <summary>
        /// Gets or sets the available application languages
        /// </summary>
        public List<Language> Languages { get; set; }

        /// <summary>
        /// Gets or sets the available networks
        /// </summary>
        public ObservableCollection<Network> Networks { get; set; }

        /// <summary>
        /// Gets the application about command
        /// </summary>
        public ICommand AboutCommand { get; }

        /// <summary>
        /// Gets the author donation command
        /// </summary>
        public ICommand DonateCommand { get; }

        /// <summary>
        /// Gets the languages dropdown items command
        /// </summary>
        public ICommand LanguageDropDownMenuItemCommand { get; }

        /// <summary>
        /// Gets the show/hide password command
        /// </summary>
        public ICommand ShowHidePasswordCommand { get; }

        /// <summary>
        /// Gets the network export command
        /// </summary>
        public ICommand ExportNetworkCommand { get; }

        /// <summary>
        /// Gets the network import command
        /// </summary>
        public ICommand ImportNetworkCommand { get; }

        /// <summary>
        /// Gets the network delete command
        /// </summary>
        public ICommand DeleteNetworkCommand { get; }

        /// <summary>
        /// Gets the scan for networks command
        /// </summary>
        public ICommand ScanNetworksCommand { get; set; }

        /// <summary>
        /// Gets the save networks command
        /// </summary>
        public ICommand SaveNetworksCommand { get; }

        /// <summary>
        /// Gets the close application command
        /// </summary>
        public ICommand CloseCommand { get; }

        /// <summary>
        /// Gets all networks on device
        /// </summary>
        /// <returns></returns>
        public async Task GetNetworks()
        {
            MetroDialogSettings dialogSettings = new MetroDialogSettings()
            {
                NegativeButtonText = LanguageManager.GetTranslated(Constants.Cancel),
                AnimateShow = false,
                AnimateHide = false
            };

            var controller = await dialogCoordinator.ShowProgressAsync(this, LanguageManager.GetTranslated("PleaseWait"), LanguageManager.GetTranslated("Scanning"), settings: dialogSettings);
            await Task.Delay(250);
            controller.SetCancelable(true);

            var networkResult = await NetworkManager.GetNetworkProfiles();
            if (networkResult.IsValid)
            {
                int networksCount = networkResult.Data.Count;
                int counter = 0;

                foreach (Network network in networkResult.Data)
                {
                    if (controller.IsCanceled) break;

                    counter++;

                    double value = (double)counter / networksCount;
                    controller.SetProgress(value);
                    controller.SetMessage(string.Format(LanguageManager.GetTranslated("ScanningNetwork"), counter, networksCount, Environment.NewLine));

                    network.SetNetworkProfile();
                    Networks.Add(network);

                    await Task.Delay(20);
                }

                await controller.CloseAsync();

                if (controller.IsCanceled)
                {
                    await dialogCoordinator.ShowMessageAsync(this, LanguageManager.GetTranslated(Constants.NetworkManager), LanguageManager.GetTranslated("ScanningCancelled"), settings: ErrorDialogSettings);
                }

                if (counter > 0)
                {
                    //NetworksGrid.Focus();
                    return;
                }

                await dialogCoordinator.ShowMessageAsync(this, LanguageManager.GetTranslated(Constants.NetworkManager), LanguageManager.GetTranslated("ScanningCompleted"), settings: ErrorDialogSettings);
            }
            else
            {
                await controller.CloseAsync();
                await dialogCoordinator.ShowMessageAsync(this, LanguageManager.GetTranslated(networkResult.Error?.Key), LanguageManager.GetTranslated(networkResult.Error?.Value), settings: ErrorDialogSettings);
            }
        }

        /// <summary>
        /// Gets an error dialog settings
        /// </summary>
        /// <returns></returns>
        private MetroDialogSettings ErrorDialogSettings => new MetroDialogSettings()
        {
            AffirmativeButtonText = LanguageManager.GetTranslated(Constants.OK)
        };
    }
}
