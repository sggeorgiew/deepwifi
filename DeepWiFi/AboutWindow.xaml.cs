﻿using DeepWiFi.Tools;
using DeepWiFi.ViewModels;
using MahApps.Metro.Controls;
using System.Windows;

namespace DeepWiFi
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : MetroWindow
    {
        private readonly AboutWindowViewModel viewModel;

        public AboutWindow()
        {
            viewModel = new AboutWindowViewModel();
            DataContext = viewModel;
            
            InitializeComponent();

            Title = $"DeepWiFi {LanguageManager.GetTranslated("About")}";
        }

        public AboutWindow(Window owner) : this()
        {
            Owner = owner;
        }
    }
}
