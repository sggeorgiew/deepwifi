﻿using DeepWiFi.Common;
using DeepWiFi.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DeepWiFi.Tools
{
    /// <summary>
    /// Network profiles commands
    /// </summary>
    public class NetworkManager
    {
        /// <summary>
        /// Constant value of netsh command for networks list
        /// </summary>
        private const string command = "netsh wlan show profiles";

        /// <summary>
        /// Gets all available network profiles of device
        /// </summary>
        /// <returns></returns>
        public async static Task<ResultData<ICollection<Network>>> GetNetworkProfiles()
        {
            ICollection<Network> networks = null;

            try
            {
                var result = await command.Bash();
                if (result != null)
                {
                    if (result.Key == Stage.Completed)
                    {
                        string[] lines = result.Value?.Split('\n');
                        int networkCounter = 0;

                        if (lines != null)
                        {
                            networks = new Collection<Network>();

                            foreach (string line in lines)
                            {
                                if (line == "\r") continue;

                                string trimmedLine = line.Trim();
                                if (trimmedLine.StartsWith("All User Profile"))
                                {
                                    string[] elements = trimmedLine.Split(':');
                                    string networkName = elements.LastOrDefault()?.Trim();

                                    if (!string.IsNullOrEmpty(networkName))
                                    {
                                        Network network = new Network()
                                        {
                                            Id = ++networkCounter,
                                            SSID = networkName,
                                            Password = "Unknown"
                                        };

                                        networks.Add(network);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return ResultData<ICollection<Network>>.BadResponse(Constants.NetworkManager, result.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return ResultData<ICollection<Network>>.BadResponse(Constants.NetworkManager, ex.Message);
            }

            return ResultData<ICollection<Network>>.CorrectResponse(networks);
        }

        /// <summary>
        /// Gets a detailed information about network profile
        /// </summary>
        /// <param name="network"></param>
        public async static void GetNetworkInformation(Network network)
        {
            string command = $"netsh wlan show profiles \"{network.SSID}\" key=clear";

            var result = await command.Bash();
            if (result != null)
            {
                string[] lines = result.Value?.Split('\n');
                if (lines != null)
                {
                    string connectionMode = null, authentication = null, networkPassword = null;

                    foreach (string line in lines)
                    {
                        if (!line.Contains(':')) continue;

                        string trimmedLine = line.Trim();
                        if (trimmedLine.StartsWith("Connection mode"))
                        {
                            connectionMode = GetValue(trimmedLine);
                        }
                        else if (trimmedLine.StartsWith("Authentication"))
                        {
                            authentication = GetValue(trimmedLine);
                        }
                        else if (trimmedLine.StartsWith("Key Content"))
                        {
                            networkPassword = GetValue(trimmedLine);
                        }
                    }

                    network.Connection = connectionMode != null && connectionMode.Contains("automatically") ? ConnectionMode.Automatically : ConnectionMode.Manually;
                    network.Authentication = GetNetworkAuthentication(authentication);
                    network.Password = networkPassword;
                }
            }
        }

        /// <summary>
        /// Exports a network profile
        /// </summary>
        /// <param name="network"></param>
        /// <returns></returns>
        public async static Task<ResultData<bool>> ExportNetworkProfile(Network network, string directory = null)
        {
            if (string.IsNullOrWhiteSpace(directory))
            {
                directory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }

            string command = $"netsh wlan export profile name=\"{network.SSID}\" key=clear folder=\"{directory}\"";

            var result = await command.Bash();
            if (result != null)
            {
                if (result.Key != Stage.Completed)
                {
                    return ResultData<bool>.BadResponse(Constants.NetworkManager, result.Value);
                }
            }

            return ResultData<bool>.CorrectResponse(true);
        }

        /// <summary>
        /// Imports a network profile
        /// </summary>
        /// <param name="networkFile"></param>
        /// <returns></returns>
        public async static Task<ResultData<Network>> ImportNetworkProfile(string networkFile)
        {
            if (string.IsNullOrWhiteSpace(networkFile))
            {
                return ResultData<Network>.BadResponse(Constants.NetworkManager, "ImportMissingFilePath");
            }

            if (!File.Exists(networkFile) || Path.GetExtension(networkFile) != ".xml")
            {
                return ResultData<Network>.BadResponse(Constants.NetworkManager, "ImportIncorrectOrNotExistingFile");
            }

            string command = $"netsh wlan add profile filename=\"{networkFile}\"";

            var result = await command.Bash();
            if (result != null)
            {
                if (result.Key != Stage.Completed)
                {
                    return ResultData<Network>.BadResponse(Constants.NetworkManager, result.Value);
                }
            }

            // Loads the network XML file profile
            XElement xElement = XElement.Load(networkFile);

            // Gets the element that contains the name of the network
            var name = xElement.Descendants().FirstOrDefault(x => x.Name.LocalName == "name");
            if (name != null && !string.IsNullOrWhiteSpace(name.Value))
            {
                Network network = new Network()
                {
                    SSID = name.Value
                };

                return ResultData<Network>.CorrectResponse(network);
            }

            return ResultData<Network>.CorrectResponse(null);
        }

        /// <summary>
        /// Deletes a network profile
        /// </summary>
        /// <param name="network"></param>
        /// <returns></returns>
        public async static Task<ResultData<bool>> DeleteNetworkProfile(Network network)
        {
            string command = $"netsh wlan delete profile name=\"{network.SSID}\"";

            var result = await command.Bash();
            if (result != null)
            {
                if (result.Key != Stage.Completed)
                {
                    return ResultData<bool>.BadResponse(Constants.NetworkManager, result.Value);
                }
            }

            return ResultData<bool>.CorrectResponse(true);
        }

        /// <summary>
        /// Gets a specific value
        /// </summary>
        /// <param name="source"></param>
        /// <param name="contains"></param>
        /// <returns></returns>
        private static string GetValue(string source, string contains = ":")
        {
            if (source != null && source.Contains(contains))
            {
                int startIndex = source.IndexOf(contains) + contains.Length + 1;
                int endIndex = source.Length;

                return source[startIndex..endIndex];
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets a network authentication mode
        /// </summary>
        /// <param name="authentication"></param>
        /// <returns></returns>
        private static Authentication GetNetworkAuthentication(string authentication)
        {
            return authentication switch
            {
                "Open" => Authentication.Open,
                "WEP" => Authentication.WEP,
                "WPA-Personal" => Authentication.WPAPersonal,
                "WPA-Enterprice" => Authentication.WPAEnterprice,
                "WPA2-Personal" => Authentication.WPA2Personal,
                "WPA2-Enterprice" => Authentication.WPA2Enterprice,
                _ => Authentication.Unknown,
            };
        }
    }
}
