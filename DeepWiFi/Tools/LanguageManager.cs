﻿using DeepWiFi.Common;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;

namespace DeepWiFi.Tools
{
    public static class LanguageManager
    {
        /// <summary>
        /// Gets the current application language
        /// </summary>
        public static string CurrentLanguage { get => Thread.CurrentThread.CurrentCulture.Name; }

        /// <summary>
        /// Changes the application language
        /// </summary>
        /// <param name="culture">Language key culture</param>
        public static void ChangeLanguage(string culture)
        {
            if (string.IsNullOrEmpty(culture)) return;

            // Copy all MergedDictionarys into an auxiliar list
            var dictionaryList = Application.Current.Resources.MergedDictionaries.ToList();

            // Search for the specified culture
            string requestedCulture = $"i18n/Lang.{culture}.xaml";
            ResourceDictionary resourceDictionary = dictionaryList.FirstOrDefault(x => x.Source.OriginalString == requestedCulture);

            if (resourceDictionary == null)
            {
                // If not found, select our default language
                requestedCulture = "i18n/Lang.en-US.xaml";
                resourceDictionary = dictionaryList.FirstOrDefault(x => x.Source.OriginalString == requestedCulture);
            }

            // If we have the requested resource, remove it from the list and place at the end
            // Then this language will be our string table to use
            if (resourceDictionary != null)
            {
                Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }

            // Inform the threads of the new culture
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }

        /// <summary>
        /// Gets a translated value of key for current language
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetTranslated(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                return Application.Current.Resources[Constants.Unknown]?.ToString();

            return Application.Current.Resources[key]?.ToString() ?? key;
        }
    }
}
