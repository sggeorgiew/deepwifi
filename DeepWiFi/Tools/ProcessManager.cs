﻿using System.Diagnostics;

namespace DeepWiFi.Tools
{
    /// <summary>
    /// Generic process manager allows you to start a windows process
    /// </summary>
    public static class ProcessManager
    {
        /// <summary>
        /// Starts a specified windows process
        /// </summary>
        /// <param name="value">Process to start/open</param>
        public static void Open(string value)
        {
            Process process = new Process
            {
                StartInfo = new ProcessStartInfo(value)
                {
                    UseShellExecute = true
                }
            };

            process.Start();
        }
    }
}
