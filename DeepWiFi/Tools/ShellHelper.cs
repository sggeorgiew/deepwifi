﻿using DeepWiFi.Common;
using DeepWiFi.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace DeepWiFi.Tools
{
    /// <summary>
    /// Shell bash helper
    /// </summary>
    public static class ShellHelper
    {
        /// <summary>
        /// Executes the <see cref="string"/> as a bash command
        /// </summary>
        /// <param name="cmd">Bash command</param>
        /// <returns><see cref="int"/> with the result code; 0 for success</returns>
        public static Task<KeyValue<Stage, string>> Bash(this string cmd)
        {
            var source = new TaskCompletionSource<KeyValue<Stage, string>>();

            Process process = new Process
            {
                StartInfo = new ProcessStartInfo("cmd", $"/c {cmd}")
                {
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                },
                EnableRaisingEvents = true
            };

            process.Exited += (sender, args) =>
            {
                if (process.ExitCode == 0)
                {
                    source.SetResult(new KeyValue<Stage, string>(Stage.Completed, process.StandardOutput.ReadToEnd()));
                }
                else
                {
                    source.SetResult(new KeyValue<Stage, string>(Stage.Failed, process.StandardOutput.ReadToEnd()));
                    //source.SetException(new Exception($"Command `{cmd}` failed with exit code `{process.ExitCode}`"));
                }

                process.Dispose();
            };

            try
            {
                process.Start();
            }
            catch (Exception ex)
            {
                source.SetException(ex);
            }

            return source.Task;
        }
    }
}
