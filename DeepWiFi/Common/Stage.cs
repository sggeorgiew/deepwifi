﻿namespace DeepWiFi.Common
{
    /// <summary>
    /// Available network stages
    /// </summary>
    public enum Stage
    {
        Failed,
        Completed
    }
}
