﻿namespace DeepWiFi.Common
{
    /// <summary>
    /// WiFi Authentication mode
    /// </summary>
    public enum Authentication
    {
        Unknown,
        Open,
        WEP,
        WPAPersonal,
        WPAEnterprice,
        WPA2Personal,
        WPA2Enterprice
    }
}
