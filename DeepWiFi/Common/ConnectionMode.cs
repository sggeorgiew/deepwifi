﻿namespace DeepWiFi.Common
{
    /// <summary>
    /// WiFi Connection mode
    /// </summary>
    public enum ConnectionMode
    {
        /// <summary>
        /// Unknown connection mode
        /// </summary>
        Unknown,

        /// <summary>
        /// Manual connection
        /// </summary>
        Manually,

        /// <summary>
        /// Automatic connection
        /// </summary>
        Automatically
    }
}
