﻿namespace DeepWiFi.Common
{
    /// <summary>
    /// Global constant values
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Gets the help directory path
        /// </summary>
        public const string HelpDirectory = "./Help";

        /// <summary>
        /// Gets the NetworkManager value
        /// </summary>
        public const string NetworkManager = "NetworkManager";

        /// <summary>
        /// Gets the default value for not available status/state
        /// </summary>
        public const string NotAvailable = "NotAvailable";

        /// <summary>
        /// Gets the default value for unknown status/state
        /// </summary>
        public const string Unknown = "Unknown";

        /// <summary>
        /// Gets the no password resource value
        /// </summary>
        public const string NoPassword = "NoPassword";

        /// <summary>
        /// Gets the OK resource value
        /// </summary>
        public const string OK = "OK";

        /// <summary>
        /// Gets the cancel resource value
        /// </summary>
        public const string Cancel = "Cancel";

        /// <summary>
        /// Gets the show passwords resource value
        /// </summary>
        public const string ShowHidePasswords = "ShowHidePasswords";

        /// <summary>
        /// Gets the default value for something went wrong error 
        /// </summary>
        public const string SomethingWentWrong = "SomethingWentWrong";

        /// <summary>
        /// Gets the DeepWifi title text
        /// </summary>
        public const string DeepWiFi = 
            "─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────\n" +
            "─████████████───██████████████─██████████████─██████████████─██████──────────██████─██████████─██████████████─██████████─\n" +
            "─██░░░░░░░░████─██░░░░░░░░░░██─██░░░░░░░░░░██─██░░░░░░░░░░██─██░░██──────────██░░██─██░░░░░░██─██░░░░░░░░░░██─██░░░░░░██─\n" +
            "─██░░████░░░░██─██░░██████████─██░░██████████─██░░██████░░██─██░░██──────────██░░██─████░░████─██░░██████████─████░░████─\n" +
            "─██░░██──██░░██─██░░██─────────██░░██─────────██░░██──██░░██─██░░██──────────██░░██───██░░██───██░░██───────────██░░██───\n" +
            "─██░░██──██░░██─██░░██████████─██░░██████████─██░░██████░░██─██░░██──██████──██░░██───██░░██───██░░██████████───██░░██───\n" +
            "─██░░██──██░░██─██░░░░░░░░░░██─██░░░░░░░░░░██─██░░░░░░░░░░██─██░░██──██░░██──██░░██───██░░██───██░░░░░░░░░░██───██░░██───\n" +
            "─██░░██──██░░██─██░░██████████─██░░██████████─██░░██████████─██░░██──██░░██──██░░██───██░░██───██░░██████████───██░░██───\n" +
            "─██░░██──██░░██─██░░██─────────██░░██─────────██░░██─────────██░░██████░░██████░░██───██░░██───██░░██───────────██░░██───\n" +
            "─██░░████░░░░██─██░░██████████─██░░██████████─██░░██─────────██░░░░░░░░░░░░░░░░░░██─████░░████─██░░██─────────████░░████─\n" +
            "─██░░░░░░░░████─██░░░░░░░░░░██─██░░░░░░░░░░██─██░░██─────────██░░██████░░██████░░██─██░░░░░░██─██░░██─────────██░░░░░░██─\n" +
            "─████████████───██████████████─██████████████─██████─────────██████──██████──██████─██████████─██████─────────██████████─\n" +
            "─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────\n";
    }
}
