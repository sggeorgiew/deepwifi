﻿using DeepWiFi.Tools;
using DeepWiFi.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Windows;

namespace DeepWiFi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly MainWindowViewModel viewModel;

        public MainWindow()
        {
            viewModel = new MainWindowViewModel(DialogCoordinator.Instance);
            DataContext = viewModel;

            LanguageManager.ChangeLanguage(Properties.Settings.Default.SelectedLanguage ?? "en-US");

            InitializeComponent();

            NetworksGrid.ItemsSource = viewModel.Networks;

            Loaded += WindowLoaded;
        }

        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            await viewModel.GetNetworks();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}
