﻿using DeepWiFi.Common;
using DeepWiFi.Tools;
using DeepWiFi.ViewModels;

namespace DeepWiFi.Models
{
    public class Network : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Network"/> class
        /// </summary>
        public Network()
        {
            IsPasswordHidden = true;
        }

        /// <summary>
        /// Gets or sets the id of network
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the SSID of network
        /// </summary>
        public string SSID { get; set; }

        /// <summary>
        /// Gets or sets the password of network
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Containts the network authentication mode
        /// </summary>
        private Authentication authentication = Authentication.Unknown;

        /// <summary>
        /// Gets or sets the network authentication mode
        /// </summary>
        public Authentication Authentication
        {
            get => authentication;
            set => Set(ref authentication, value);
        }

        /// <summary>
        /// Containts the network connection type
        /// </summary>
        private ConnectionMode connection = ConnectionMode.Unknown;

        /// <summary>
        /// Gets or sets the network connection type
        /// </summary>
        public ConnectionMode Connection
        {
            get => connection;
            set
            {
                connection = value;
                OnPropertyChanged("DisplayConnection");
            }
        }

        /// <summary>
        /// Gets the friendly display value of the network connection type
        /// </summary>
        public string DisplayConnection => LanguageManager.GetTranslated(connection.ToString());

        /// <summary>
        /// Containts the hidden state of network password
        /// </summary>
        private bool isPasswordHidden;

        /// <summary>
        /// Gets or sets the hidden state of network password
        /// </summary>
        public bool IsPasswordHidden
        {
            get => isPasswordHidden;
            set => Set(ref isPasswordHidden, value);
        }

        /// <summary>
        /// Sets an information profile of network
        /// </summary>
        public void SetNetworkProfile()
        {
            NetworkManager.GetNetworkInformation(this);
        }

        /// <summary>
        /// Gets the password value
        /// </summary>
        /// <returns>Current network password</returns>
        public string GetPassword()
        {
            return Password;
        }

        /// <summary>
        /// Translates the texts of network
        /// </summary>
        public void Translate()
        {
            OnPropertyChanged("DisplayConnection");
        }
    }
}
