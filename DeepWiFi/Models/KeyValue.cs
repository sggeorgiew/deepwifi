﻿namespace DeepWiFi.Models
{
    /// <summary>
    /// Generic key value pair
    /// </summary>
    public class KeyValue<KeyType, ValueType>
    {
        /// <summary>
        /// Gets or sets the key in the key/value pair
        /// </summary>
        public KeyType Key { get; set; }

        /// <summary>
        /// Gets or sets the value in the key/value pair
        /// </summary>
        public ValueType Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValue"/> class
        /// </summary>
        /// <param name="key">Key from the key/value pair</param>
        /// <param name="value">Value from the key/value pair</param>
        public KeyValue(KeyType key, ValueType value)
        {
            Key = key;
            Value = value;
        }
    }

    /// <summary>
    /// Generic key value pair
    /// </summary>
    public class KeyValue<Type> : KeyValue<Type, Type>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValue"/> class
        /// </summary>
        /// <param name="key">Key from the key/value pair</param>
        /// <param name="value">Value from the key/value pair</param>
        public KeyValue(Type key, Type value) : base(key, value)
        { }
    }

    /// <summary>
    /// Generic key value pair
    /// </summary>
    public class KeyValue : KeyValue<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValue"/> class
        /// </summary>
        /// <param name="key">Key from the key/value pair</param>
        /// <param name="value">Value from the key/value pair</param>
        public KeyValue(string key, string value) : base(key, value)
        { }
    }
}
