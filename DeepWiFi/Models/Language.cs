﻿using DeepWiFi.Tools;
using DeepWiFi.ViewModels;

namespace DeepWiFi.Models
{
    /// <summary>
    /// Application language data
    /// </summary>
    public class Language : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the key of language
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the short name of language
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets whether the language is available for select (selectable)
        /// </summary>
        public bool IsSelectable => Key != LanguageManager.CurrentLanguage;

        /// <summary>
        /// Initializes a new instance of the <see cref="Language"/> class
        /// </summary>
        /// <param name="key">Key of the language</param>
        /// <param name="name">Short name of the language</param>
        public Language(string key, string name)
        {
            Key = key;
            Name = name;
        }

        public void UpdateStatus()
        {
            OnPropertyChanged("IsSelectable");
        }
    }
}
