﻿namespace DeepWiFi.Models
{
    /// <summary>
    /// Global result data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultData<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultData"/> class
        /// </summary>
        /// <param name="isValid"></param>
        /// <param name="errorKey"></param>
        /// <param name="errorMessage"></param>
        private ResultData(bool isValid, string errorKey = null, string errorMessage = null)
        {
            IsValid = isValid;

            if (!isValid)
            {
                Error = new KeyValue(errorKey, errorMessage);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultData"/> class
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isValid"></param>
        /// <param name="errorKey"></param>
        /// <param name="errorMessage"></param>
        private ResultData(T data, bool isValid, string errorKey = null, string errorMessage = null) : this(isValid, errorKey, errorMessage)
        {
            Data = data;
        }

        /// <summary>
        /// Gets or sets the result data
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Gets or sets whether the result is valid or not
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets the error key/value pair
        /// </summary>
        public KeyValue Error { get; set; }

        /// <summary>
        /// Generates a bad result response
        /// </summary>
        /// <param name="errorKey"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ResultData<T> BadResponse(string errorKey, string errorMessage)
        {
            return new ResultData<T>(default, false, errorKey, errorMessage);
        }

        /// <summary>
        /// Generates a correct result response
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ResultData<T> CorrectResponse(T data)
        {
            return new ResultData<T>(data, true);
        }
    }
}
